rm(list=ls());
source("~/mybiotools/r/myfunc.R");
options("scipen"=1, "digits"=4, stringsAsFactors=FALSE);

source("/gpfs/ycga/project/fas/xu_ke/xz345/work/other/scdream/scripts/common.R", echo=T)

#removing y < 0 training samples
rem.i = which( geometry$ycoord < 0 )
bin.bdt2 = bin.bdt[-rem.i, ]
cont.bdt2 = cont.bdt[-rem.i, ]
geometry2 = geometry[-rem.i,]

#x, y, z split point to produce categorical response
x.split = c(min(geometry2$xcoord)-100, summary((geometry2$xcoord))[2], 0, summary((geometry2$xcoord))[5], max(geometry2$xcoord)+100)
y.split = c(min(geometry2$ycoord)-100, summary((geometry2$ycoord))[2], summary(geometry2$ycoord)[3], summary((geometry2$ycoord))[5], max(geometry2$ycoord)+100) #using median
z.split = c(min(geometry2$zcoord)-100, summary((geometry2$zcoord))[2], 0, summary((geometry2$zcoord))[5], max(geometry2$zcoord)+100)

geometry2factor.all <- function(geometry2) {
#no cut, using all as factor
	factor.y = paste("X", geometry2$xcoord, geometry2$ycoord, geometry2$zcoord, sep="_")
	Y = num2LetterFactor(factor.y)
}

geometry2factor <- function(geometry2, x.split, y.split, z.split) {
	x.l = cut (geometry2$xcoord, x.split) # two levels
	levels(x.l) = paste0("X", 1:length(levels(x.l)))
	y.l = cut (geometry2$ycoord, y.split) # two levels
	levels(y.l) = paste0("Y", 1:length(levels(x.l)))
	z.l = cut (geometry2$zcoord, z.split) # two levels
	levels(z.l) = paste0("Z", 1:length(levels(x.l)))

	which(is.na(x.l))
	which(is.na(y.l))
	which(is.na(z.l))
	table(x.l, y.l, z.l)
	factor.y = paste(x.l, y.l, z.l, sep="")
	Y = num2LetterFactor(factor.y)
}

#Y = geometry2factor(geometry2, x.split, y.split, z.split)
Y = geometry2factor.all(geometry2)
table(Y)

## train.dat
####binary
X = bin.bdt2
X = data.frame(X)
X$response = Y
train_out_rdata = "train.dat.bin.rdata"
train.dat = X
save(train.dat, file = train_out_rdata)

####cont
X = cont.bdt2
X = data.frame(X)
X$response = Y
train.dat = X
train_out_rdata = "train.dat.cont.rdata"
save(train.dat, file = train_out_rdata)

#Y = geometry2factor(DistMap_coor, x.split, y.split, z.split)
Y = geometry2factor.all(DistMap_coor)
table(Y)

## test.XY
####binary
X = t(bin.sc.84g)
save(X, Y, file="testing.XY.bin.rdata")
X.bin = X

####cont
genes = colnames(bin.bdt)
X = t(sc.norm.quant)[, genes]
dim(X)
(rownames(X) = rownames(X.bin))
save(X, Y, file="testing.XY.cont.rdata")


