#!/usr/bin/env perl

use strict;
use Data::Dumper;
use Carp qw(confess);
use File::Basename;
use Getopt::Long;
use my_usual;

my $script = basename($0);

my $CMD = "Rscript /gpfs/ycga/project/fas/xu_ke/xz345/soft/git/scdream/scripts/gene_perm3_meanDist.R ";

my $start = shift || 1;
my $end = shift || 20;
my $num = shift || 10000;
my $CPUnum = shift || 10;
my $pmem = $CPUnum * 6000;

my @bin = qw/T F/;
for my $ifbin (@bin) {
	for my $i ($start..$end) {
		my $max = $i * $num;
		my $min = ($i - 1) * $num + 1;
		#print ($min, '-', $max, "\n");
		my $CMD2 = "pbsv2.pl \"". $CMD . ' ' . $min . ' ' . $max . ' ' . $ifbin . ' ' . $CPUnum . '"'. ' -q general -pmem ' . $pmem . ' -ppn '. $CPUnum;
		print $CMD2, "\n";
		!system ($CMD2) or die $!;
	}
}
