rm(list=ls());
options("scipen"=1, "digits"=4, stringsAsFactors=FALSE);
source("/gpfs/ycga/project/fas/xu_ke/xz345/work/other/scdream/scripts/common.R", echo=T)

gene_geom_cor <- function (expr.gene, #a vetor of gene expr 
						   geometry #x, y, z of cell position
						   ) {
	cor.x.out = cor.test(expr.gene, geometry$xcoord)
	cor.y.out = cor.test(expr.gene, geometry$ycoord)
	cor.z.out = cor.test(expr.gene, geometry$zcoord)
	R.out = abs(cor.x.out$estimate) + abs(cor.y.out$estimate) + abs(cor.z.out$estimate)
	return (R.out)
}

g.w = apply(cont.bdt,2,gene_geom_cor, geometry)
g.w.sort = sort(g.w, decreasing=T)

gene.weight = g.w.sort * 100
gene.cor.num = as.integer(gene.weight) # how many correlated genes will be got for each 84 gene
names(gene.cor.num) = names(gene.weight)

linear_corre <- function (genename, #1 name of 84 genes
						  CPUnum = 20, #threads 
						  out.num = 1, #how many top results will be returned, according to gene weight
						  sort.by = 4, #4: pv, 3: tv, 1: estimate
						  sc.dm, #data matrix, row: gene; col: cell
						  decreasing = F,
						  ... #other parameters, 
						  ) {
#Values:
#return genes that correlted with genename
	library(doParallel)
	registerDoParallel(cores = CPUnum)
	gene.i = which(rownames(sc.dm) == genename)
	stopifnot(length(gene.i) == 1)
	g.expr = as.numeric(sc.dm[gene.i,])
	d = sc.dm[-gene.i,]
	n = nrow(d)
	#blocksize = as.integer(n / (CPUnum + 1)) + 1
	blocksize = CPUnum * 2
	blocksize = ifelse (blocksize<n, blocksize, n)
	pos = seq(1, n, length.out=blocksize)
	pos[length(pos)] = n + 1
	block_num = length(pos) - 1
	#message ("Block number: ", block_num, "\n")
	header = c("Estimate", "Std. Error", "t value", "Pr(>|t|)")
	exp.cor <- foreach (j = 1:block_num, .combine = rbind) %dopar%
	{
		start = pos[j]
		end = pos[j + 1] - 1
		temp = d[start:end, ]
		out.s = c()
		for (i in 1:nrow(temp)) {
			x = as.numeric(temp[i,])
			out.v = glm(x ~ g.expr)
			out.glm = coef(summary(out.v))[2,]
			out.s = rbind(out.s, out.glm)
		}
		colnames(out.s) = header
		rownames(out.s) = rownames(temp)
		out.s
	}
	exp.cor = exp.cor[order(exp.cor[, sort.by], decreasing = decreasing),]
	rownames(exp.cor)[1:out.num]
}


if (T) {
	genename="Mes2"
	t1 = linear_corre(genename=genename, sc.dm=sc.norm.quant, out.num=gene.cor.num[genename])
	t2 = linear_corre(genename=genename, sc.dm=sc.norm, out.num=gene.cor.num[genename])
	t3 = linear_corre(genename=genename, sc.dm=sc.raw, out.num=gene.cor.num[genename])
	length(intersect(t1,t2))
	#119 out of 137
	library(VennDiagram)
	v.d = list(quantile.norm=t1, UMI.norm=t2, raw=t3)
	cols = c("#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7");
	cols = rev(cols);
	venn.plot <- venn.diagram(
							  x=v.d,
							  filename="Mes2_cor_gene.venn.png",
							  imagetype="png",
							  width = 6, height = 6, units = "in", res = 300,
							  col="transparent",
							  cat.col=cols[1:length(v.d)],
							  fill=cols[1:length(v.d)],
							  )
}

selected.gene = c()
i = 0
for (genename in names(gene.cor.num)){
	i = i + 1
	print (paste0("Gene: ", genename, "; No. ", i, "; out.gene.num: ", gene.cor.num[genename]))
	gene.out = linear_corre(genename=genename, sc.dm=sc.norm.quant, out.num=gene.cor.num[genename])
	selected.gene = c(selected.gene, gene.out)
}
selected.gene.u = unique(selected.gene)
intersect(selected.gene.u, names(bin.bdt)) # 83 genes are on the list
setdiff(names(bin.bdt), selected.gene.u) #"bmm"

#### final list of space related genes
selected.gene.u = c(selected.gene.u, setdiff(names(bin.bdt), selected.gene.u))
length(selected.gene.u) # 1109

sc.expr.space = sc.norm.quant[selected.gene.u,]
colnames(sc.expr.space) = colnames(sc.norm) 
head(sc.expr.space[,1:5])

save(sc.expr.space, g.w.sort, gene.cor.num, file = "sc.expr.space.rdata")






