#source("~/mybiotools/r/", echo=T)
#run:  pbsv2.pl -q general -ppn 20 -pmem 120000 -e -wt 7- "runR.sh /gpfs/ycga/project/fas/xu_ke/xz345/soft/git/scdream/scripts/MLv5cvMRF_ga.R"
#post: pbsv2.pl -q general -ppn 20 -pmem 120000 "Rscript --slave ~/scdream/scripts/eval_genes.R g60/GA_glmnet_maxiter1000_g60_run20_popSize120_MLMRF_tree5_ga.rdata > g60.txt"

rm(list=ls());
options("scipen"=1, "digits"=4, stringsAsFactors=FALSE);

#library(caret)
library(GA)
library(MultivariateRandomForest)
#library(pROC)

source("./make_cluster_para.R")

method = "MRF"
out.rdata.file = paste0("GA_glmnet_maxiter", maxiter, "_g", select.num, "_run", run, "_popSize", popSize, "_ML", method, "_tree", n_tree, "_ga.rdata")
out.pdf = 		 paste0("GA_glmnet_maxiter", maxiter, "_g", select.num, "_run", run, "_popSize", popSize, "_ML", method, "_tree", n_tree, "_ga.pdf")

seed = select.num + popSize + maxiter + run
#ifparallel = cl
ifparallel = T
#save (cl, file="cl.rdata")

source("/gpfs/ycga/project/fas/xu_ke/xz345/soft/git/scdream/scripts/common.R")
source("/gpfs/ycga/project/fas/xu_ke/xz345/work/other/scdream/scripts/build_forest_predict_hpc_fork.R")

genes.84 = names(bin.bdt)
cont.sc.84g = sc.norm.quant[genes.84,]
geometry1 = geometry

nBits = length(genes.84)


fitness.ml <- function (bin) {
	#debug
	# bin = sample(c(rep(0, 24), rep(1, 60)), replace = F)
	# load("~/GA/g20_ifbinTRUE_ga.rdata")
	# bin = GA@solution
	s.i = which(bin == 1)
	#Y.test = cbind(xcoord=Y1, ycoord=Y2, zcoord=Y3)
	genes = genes.84[s.i]

	if (is.null(m_feature)) m_feature = length(genes) - 1
	if (is.null(min_leaf)) min_leaf = 1000

	X.training = as.matrix(t(cont.sc.84g)[, genes])
	Y.training = as.matrix(DistMap_coor)
	X.test =  X.training

	mapping_position <- build_forest_predict_hpc_fork(
													  X.training,
													  Y.training,
													  n_tree = n_tree,
													  seed = seed,
													  m_feature = m_feature,
													  min_leaf = min_leaf,
													  CPUnum = CPUnum,
													  cv.fold = cv.fold,
													  testX = X.training
													  )

	rv = 0
	for (i in 1:3) {
		rv2 = cor(mapping_position[,i], DistMap_coor[,i])
		rv = rv + rv2
	}
	rv/3

	#-1*evalu.out$scoring
}

#for ga relative functions
source("/gpfs/ycga/project/fas/xu_ke/xz345/soft/git/scdream/R/ga_func.R")

####
#clusterExport(cl, varlist = c("cont.sc.84g", "genes.84", "min_leaf", "m_feature", "n_tree", "CPUnum", "DistMap_coor_new", "bin.sc.84g", "geometry1", "DistMap_coor", "fitness.ml", "initial_population", "monitor_check_genenum", "monitor_check_genenum", "select_pop", "crossover_pop", "mutat_pop"))
#clusterCall(cl, library, package = "caret", character.only = TRUE)
#clusterCall(cl, library, package = "MultivariateRandomForest", character.only = TRUE)
####

GA <- ga(
		 type = "binary",
		 fitness = fitness.ml,
		 population = initial_population,
		 monitor = monitor_check_genenum,
		 selection = select_pop,
		 crossover = crossover_pop,
		 mutation = mutat_pop,
		 popSize = popSize,
		 keepBest = keepBest,
		 parallel = ifparallel,
		 maxiter = maxiter,
		 run =  run,
		 seed = seed,
		 maxFitness = maxFitness,
		 pmutation = pmutation,
		 pcrossover = pcrossover,
		 #method = method,
		 nBits = nBits
		 )

summary(GA)
s = GA@solution
rowSums(s)
s = GA@population
rowSums(s)

if (!debug) {
	save (GA, file=out.rdata.file)
	pdf(out.pdf)
	plot(GA)
	dev.off()
}
