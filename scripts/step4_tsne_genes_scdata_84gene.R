rm(list=ls());
source("~/mybiotools/r/myfunc.R");
options("scipen"=1, "digits"=4, stringsAsFactors=FALSE);

source("/gpfs/ycga/project/fas/xu_ke/xz345/work/other/scdream/scripts/common.R", echo=T)

#library(scdream)
#data("geometry")

load("~/scdream/method2/sc.expr.space.rdata")
genes = names(gene.cor.num)
X= t(sc.expr.space[genes,])

#process X to better draw
if(F) {
	X.v = as.vector(X)
	X.s = sort(unique(X.v))
	if (X.s[1] == 0) {
		low.limit = X.s[2] #first non-zero value
		X[which(X<low.limit)] = low.limit
	} else {
		stop ("Minest is not 0")
	}
}

Y=as.factor(rep("X1", dim(X)[1]))
pdffile.kw = "headtail"
pch = c(19, 15, 16, 17, 18)
cex = 1
library(RColorBrewer)
cols = brewer.pal(n = 8, name = 'RdBu')
COLOR = c(cols[1], cols[8])
COLOR = c("darkgreen", "darkblue", COLOR)

#for side bar
sidebar.values = c()

#genes = colnames(bin.bdt)

pdffile = "scdata_gene_plot_84gene.pdf"
pdf.w = 8
pdf.h = 8

#require(parallel)
perplexitys = 8
max_iters = 6000

Rampcol = "Blues"
#cex.axis.sidebar = 1

if (F) {
#source("~/mybiotools/r/myplot_tsne_v2.R")

pdf(pdffile, pdf.w, pdf.h, family = "ArialMT")
par(mfrow = c(2, 2))
#for (g in c("Mes2")) {
#for (g in genes) {
for (g in genes[1:4]) {
	sidebar.values = as.numeric(X[, g])
	main=g
	myplot_tsne_v2(X, 
				   Y, 
				   main=main,
				   perplexitys=perplexitys,
				   max_iters=max_iters,
				   pch=pch,
				   cex=cex,
				   COLOR=COLOR,
				   sidebar.values = sidebar.values,
				   Rampcol = Rampcol,
				   )
}

dev.off()
}

require("doParallel")
threads <- 20
### debug
#threads <- 10
#max_iters = 100
#genes = genes[1:10]
###

registerDoParallel(threads)
#all.pdffiles = c()

all.pdffiles <-
	foreach (g = genes, .combine = c) %dopar%
	#foreach (g = genes[1:10], .combine = rbind) %dopar%
	{
			sidebar.values = as.numeric(X[, g])
			main=g
			pdffile.kw = paste0("gene_plot_", g)
			pdffile.t = paste0(g, "_grid_tsne.pdf")
			#all.pdffiles = c(all.pdffiles, pdffile.t)
			myplot_tsne(X, 
						Y, 
						main=main,
						pdffile.kw = g,
						perplexitys=perplexitys,
						max_iters=max_iters,
						pch=pch,
						cex=cex,
						COLOR=COLOR,
						sidebar.values = sidebar.values,
						Rampcol = Rampcol,
						)
			pdffile.t
	}

#all.pdffiles = list.files(pattern="_grid_tsne")
CMD = paste0("convert ", paste(all.pdffiles, collapse=" "), " ", pdffile)
print (CMD)
system (CMD)
CMD = paste0("rm -f ", paste(all.pdffiles, collapse=" "))
print (CMD)
system (CMD)



