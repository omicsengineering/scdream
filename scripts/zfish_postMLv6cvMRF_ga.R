#!/usr/bin/env Rscript

#run:  pbsv2.pl -ppn 20 -pmem 120000 -e -wt 7- "Rscript /gpfs/ycga/project/fas/xu_ke/xz345/soft/git/scdream/scripts/zfish_postMLv6cvMRF_ga.R 3"
#run:  pbsv2.pl -ppn 10 -pmem 100000 -e -wt 7- "bash batch_10CV_ga.sh"
#post: pbsv2.pl -q general -ppn 20 -pmem 120000 "Rscript --slave ~/scdream/scripts/eval_genes.R g20/GA_glmnet_maxiter1000_g20_run20_popSize120_MLMRF_tree5_ga.rdata > g20.txt"

rm(list=ls());
options("scipen"=1, "digits"=4, stringsAsFactors=FALSE);

args<-commandArgs(T)
batch_num = as.numeric(args[1]) # batch number of 10

#library(caret)
library(GA)
library(MultivariateRandomForest)
#library(pROC)

#source("./make_cluster_para.R")
# initial cluster
#node.num = 10
queue = "general"
pmem = 120000
wt = "7-"
ppn = 20

########### setup for GA ############
select.num = 20 #
keepBest = T
popSize = 80
n_tree = 20
#m_feature=10
#min_leaf=10

######
#MRF
m_feature = 5
min_leaf = 5
cv.fold = 4
######

CPUnum = n_tree

#maxiter = 1000
maxiter = 200
run = 80
#maxiter = 100
#run =  10
maxFitness = 1
pmutation = 0.1
pcrossover = 0.8
########### setup for GA ############

#debug
debug = T
###debug###
if (F) {
	    debug = T
    maxiter = 5
	    run = 5
	    popSize = 20
}
##########


method = "MRF"
out.rdata.file = paste0("BatchNum", batch_num, "GA_maxiter", maxiter, "_g", select.num, "_run", run, "_popSize", popSize, "_ML", method, "_tree", n_tree, "_ga.rdata")
out.pdf = 		 paste0("BatchNum", batch_num, "GA_maxiter", maxiter, "_g", select.num, "_run", run, "_popSize", popSize, "_ML", method, "_tree", n_tree, "_ga.pdf")

if (file.exists(out.rdata.file)) {
	stop (paste0("File ", out.rdata.file, " exist already, so skip.\n"))
} else {
	print (paste0("Starting analysis for batch of ", batch_num, "\n"))
}

seed = select.num + popSize + maxiter + run
#ifparallel = cl
ifparallel = T
#save (cl, file="cl.rdata")

#add in 12/21/2018, for 10 batch test
#batch.d = read.csv("/gpfs/ycga/scratch60/fas/xu_ke/xz345/work/scdream/perm_dist/GA/ML_GA/postMLv6/post_challenge_community/folds_train.csv", header=F)
batch.d = read.csv("/gpfs/ycga/scratch60/xu_ke/xz345/scp/zfish/cv/folds_train.csv", header=F)
batch.training.ind = batch.d[batch_num, ]
batch.training.ind = batch.training.ind[!is.na(batch.training.ind)]

source("/gpfs/ycga/project/fas/xu_ke/xz345/soft/git/scdream/scripts/common.R")
source("/gpfs/ycga/project/fas/xu_ke/xz345/work/other/scdream/scripts/build_forest_predict_hpc_fork.R")
source("/gpfs/ycga/project/fas/xu_ke/xz345/soft/git/scdream/scripts/zfish_common.R")

#genes.84 = names(bin.bdt)
cont.sc.84g = sc.norm.quant[genes.84,]
geometry1 = geometry

nBits = length(genes.84)


fitness.ml <- function (bin) {
	#debug
	# bin = sample(c(rep(0, 24), rep(1, 60)), replace = F)
	# load("~/GA/ML_GA/MLv5/g20/GA_glmnet_maxiter1000_g20_run50_popSize80_MLMRF_tree20_ga.rdata")
	# bin = GA@solution
	s.i = which(bin == 1)
	#Y.test = cbind(xcoord=Y1, ycoord=Y2, zcoord=Y3)
	genes = genes.84[s.i]

	if (is.null(m_feature)) m_feature = length(genes) - 1
	if (is.null(min_leaf)) min_leaf = 1000

	X.training = as.matrix(t(cont.sc.84g)[batch.training.ind, genes])
	Y.training = as.matrix(DistMap_coor[batch.training.ind, ])
	X.test =  X.training

	mapping_position <- build_forest_predict_hpc_fork(
													  X.training,
													  Y.training,
													  n_tree = n_tree,
													  seed = seed,
													  m_feature = m_feature,
													  min_leaf = min_leaf,
													  CPUnum = CPUnum,
													  cv.fold = cv.fold,
													  testX = X.training
													  )

	rv = 0
	for (i in 1:3) {
		rv2 = cor(mapping_position[,i], DistMap_coor[batch.training.ind, i])
		rv = rv + rv2
	}
	rv/3

	#-1*evalu.out$scoring
}

#for ga relative functions
source("/gpfs/ycga/project/fas/xu_ke/xz345/soft/git/scdream/R/ga_func.R")

####
#clusterExport(cl, varlist = c("cont.sc.84g", "genes.84", "min_leaf", "m_feature", "n_tree", "CPUnum", "DistMap_coor_new", "bin.sc.84g", "geometry1", "DistMap_coor", "fitness.ml", "initial_population", "monitor_check_genenum", "monitor_check_genenum", "select_pop", "crossover_pop", "mutat_pop"))
#clusterCall(cl, library, package = "caret", character.only = TRUE)
#clusterCall(cl, library, package = "MultivariateRandomForest", character.only = TRUE)
####

GA <- ga(
		 type = "binary",
		 fitness = fitness.ml,
		 population = initial_population,
		 monitor = monitor_check_genenum,
		 selection = select_pop,
		 crossover = crossover_pop,
		 mutation = mutat_pop,
		 popSize = popSize,
		 keepBest = keepBest,
		 parallel = ifparallel,
		 maxiter = maxiter,
		 run =  run,
		 seed = seed,
		 maxFitness = maxFitness,
		 pmutation = pmutation,
		 pcrossover = pcrossover,
		 #method = method,
		 nBits = nBits
		 )

summary(GA)
s = GA@solution
rowSums(s)
s = GA@population
rowSums(s)

#if (!debug) {
	save (GA, file=out.rdata.file)
	pdf(out.pdf)
	plot(GA)
	dev.off()
#}
