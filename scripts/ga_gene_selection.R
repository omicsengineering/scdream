#!/usr/bin/env Rscript
#using GA to discover gene sets with min dist

rm(list=ls());
args<-commandArgs(T)

select.num = as.numeric(args[1])
ifbin = as.logical(args[2])
maxiter = as.numeric(args[3])
run = as.numeric(args[4])

########### setup for GA ############
nBits = 84
keepBest = T
#maxiter = 2000
#run = 50
popSize = 100
#select.num = 60
ifparallel = T
#ifbin = T
########### setup for GA ############

###debug###
#select.num = 20
#ifbin = T
#maxiter = 20
#run = 20
#popSize = 20
##########

out.rdata.file = paste0("g", select.num, "_ifbin", ifbin, "_ga.rdata")
out.pdf = paste0("g", select.num, "_ifbin", ifbin, "_ga.pdf")

source("~/mybiotools/r/myfunc.R")

options("scipen" = 1,
        "digits" = 4,
        stringsAsFactors = FALSE)

library(scdream)
data(bin.bdt)
data(bin.sc.84g)
data(cont.bdt)
data(geometry)
data(geometry.all)
data(sc.norm)
data(sc.norm.quant)
data(sc.raw)
data(DistMap_coor)
data(gene_subset_summary)
source("~/git/scdream/R/simi_dist_measures.R")
source("~/git/scdream/R/dist_evaluation.R")
source("~/git/scdream/scripts/mapping.R")

library(GA)
library(doParallel)
# registerDoParallel(cores = 20)

# pack = c("scatterplot3d", "rgl", "DistMap", "mccr")
# check_and_install_package(pack)
# check_and_install_package("devtools")
# data(bin.bdt)
# genes = names(bin.bdt)
genes.84 = names(bin.bdt)
cont.sc.84g = sc.norm.quant[genes.84,]
geometry1 = geometry

# performance1 <- function(genes) {
fitness.min.dist <- function(bin, bin.bdt) {
    s.i = which(bin == 1)
    genes = genes.84[s.i]    
    #for measuring gene comb's performance, No 1 of 2, Euclidean distance
    mapping_position = mapping(
        geneset_input = genes, 
        bin = ifbin,
        num_PS = 10,
        cont.sc.84g = cont.sc.84g
    )
    evalu.out = dist_evaluation(predicted_posi = mapping_position,
                                idx=T,
                                weighting=F,
                                geometry1=geometry1,
                                gold_coor=DistMap_coor)
    -1*evalu.out$scoring
}

#library(gtools)
#g.comb = combn(84, 20)


#workers <- rep(c("c25n10", "c15n10"), each = 20)
#cl <- makeCluster(workers, type = "PSOCK")
#registerDoParallel(cl)

fitness.gene.bin.num <- function(bin, bin.bdt) {
    s.i = which(bin == 1)
    genes = genes.84[s.i]
    dat = bin.bdt[, genes]
    dat$merged = apply(dat, 1, function(x) {
        paste(x, collapse = "")
    })
    bin.unique = unique(dat$merged)
    length(bin.unique)
}

initial_population <- function(ga.o) {
    popSize = ga.o@popSize
    nBits = ga.o@nBits
    out = c()
    for (j in 1:popSize) {
        s.i = sample(1:nBits, select.num, replace = F)
        l = rep(0, nBits)
        l[s.i] = 1
        out = rbind(out, l)
    }
	out
}

monitor_check_genenum <- function (object, digits = getOption("digits"), ...) 
{
	fitness <- na.exclude(object@fitness)
	sumryStat <- c(mean(fitness), max(fitness))
	sumryStat <- format(sumryStat, digits = digits)
	s = object@population
	r.sum = rowSums(s)
	cat(paste("GA | iter =", object@iter, "| Mean =", sumryStat[1], 
			  "| Best =", sumryStat[2],
			  " | rowSum =", r.sum, "\n"))
}

monitor_check_genenum2 <- function(obj) 
{ 
	contour(x1, x2, f, drawlabels = FALSE, col = grey(0.5))
	title(paste("iteration =", obj@iter), font.main = 1)
	points(obj@population, pch = 20, col = 2)
	Sys.sleep(0.2)
}


select_pop <- function (object, r, q)
{
	if (missing(r))
		r <- 2 / (object@popSize * (object@popSize - 1))
	if (missing(q))
		q <- 2 / object@popSize
	rank <-
		(object@popSize + 1) - rank(object@fitness, ties.method = "min")
	prob <- 1 + q - (rank - 1) * r
	prob <- pmin(pmax(0, prob / sum(prob)), 1, na.rm = TRUE)
	sel <-
		sample(
			   1:object@popSize,
			   size = object@popSize,
			   prob = prob,
			   replace = TRUE
			   )
	out <- list(population = object@population[sel, , drop = FALSE],
				fitness = object@fitness[sel])
	return(out)
}

crossover_pop <- function (object, parents)
{
	fitness <- object@fitness[parents]
	parents <- object@population[parents, , drop = FALSE]
	parents[1,] = as.double(parents[1,])
	parents[2,] = as.double(parents[2,])
	n <- ncol(parents)
	children <- matrix(as.double(0), nrow = 2, ncol = n)
	fitnessChildren <- rep(NA, 2)

	#crossOverPoint <- sample(0:n, size = 1)
	if(sum(parents[1,]) != sum(parents[2,])) {
		message (paste0("Warning: parent 1 has diff num of parent 2: "), sum(parents[1,]), "---", sum(parents[2,]))
		print (paste(parents[1,], collapse = ","))
		print (paste(parents[2,], collapse = ","))
	}
	select.sum = sum(parents[1,])
	crossOverPoint <- sample(0:select.sum, size = 1)

	if (crossOverPoint == 0) {
		children[1:2, ] <- parents[2:1, ]
		fitnessChildren[1:2] <- fitness[2:1]
	}
	else if (crossOverPoint == select.sum) {
		children <- parents
		fitnessChildren <- fitness
	}
	else {
		p1.i = which(parents[1, ] == 1)
		p2.i = which(parents[2, ] == 1)
		c1.i = c(p1.i[1:crossOverPoint], p2.i[(crossOverPoint + 1):select.sum])
		c2.i = c(p2.i[1:crossOverPoint], p1.i[(crossOverPoint + 1):select.sum])
		#cat ("crossOverPoint: ", crossOverPoint, "\n")
		#cat ("Pre crossover parent: ", length(p1.i), " || " , length(p2.i), "\n")
		#cat ("Pre crossover child:  ", length(c1.i), " || " , length(c2.i), "\n")

		#process duplication
		dup.c1 = sum(duplicated(c1.i))
		dup.c2 = sum(duplicated(c2.i))
		if (dup.c1>0) {
			pool = setdiff(c(p1.i, p2.i), c1.i)
			c1.i = c(unique(c1.i), pool[1:dup.c1])            
		}
		if (dup.c2 > 0) {
			pool = setdiff(c(p1.i, p2.i), c2.i)
			c2.i = c(unique(c2.i), pool[1:dup.c2])
		}

		children[1, c1.i]  = 1
		children[2, c2.i]  = 1
		#cat ("Post crossover: ", length(c1.i), " || " , length(c2.i), "\n")
		stopifnot(length(c1.i) == select.sum)
		stopifnot(length(c2.i) == select.sum)
	}
	out <- list(children = children, fitness = fitnessChildren)
	return(out)
}

mutat_pop <- function (object, parent)
{
	mutate <- parent <- as.vector(object@population[parent,])
	# n <- length(parent)
	# j <- sample(1:n, size = 1)

	# select.sum = sum(parent)
	select.i = which(parent == 1)
	unselect.i = which(parent != 1)
	mutPoint.select <- sample(select.i, size = 1)
	mutPoint.unselect <- sample(unselect.i, size = 1)

	mutate[mutPoint.select] <- abs(mutate[mutPoint.select] - 1)
	mutate[mutPoint.unselect] <- abs(mutate[mutPoint.unselect] - 1)
	return(mutate)
}

GA <- ga(
		 type = "binary",
		 fitness = fitness.min.dist,
		 #fitness = fitness.gene.bin.num,
		 population = initial_population,
		 selection = select_pop,
		 crossover = crossover_pop,
		 mutation = mutat_pop,
		 popSize = popSize,
		 keepBest = keepBest,
		 parallel = ifparallel,
		 maxiter = maxiter,
		 run = 	run,
		 seed = 10000,
		 nBits = nBits,
		 monitor = monitor_check_genenum,
		 bin.bdt = bin.bdt
		 )
summary(GA)
s = GA@solution
rowSums(s)
s = GA@population
rowSums(s)

save (GA, file=out.rdata.file)

pdf(out.pdf)
plot(GA)
dev.off()

