#!/usr/bin/env Rscript
#using GA / tsne to find best gene sets among all 9000 genes

rm(list=ls());
args<-commandArgs(T)

#select.num = as.numeric(args[1])
#ifbin = as.logical(args[2])
maxiter = as.numeric(args[1])
run = as.numeric(args[2])
popSize = as.numeric(args[3])

########### setup for GA ############
#nBits = 84
keepBest = T
#popSize = 200
ifparallel = T
########### setup for GA ############

###debug###
#maxiter = 2
#run = 2
#popSize = 20
##########

out.pdf = 		 paste0("tsneGA_All", "_maxiter", maxiter, "_run", run, "_popSize", popSize, "_ga.pdf")
out.rdata.file = paste0("tsneGA_All", "_maxiter", maxiter, "_run", run, "_popSize", popSize, "_ga.rdata")

source("~/mybiotools/r/myfunc.R")

options("scipen" = 1,
        "digits" = 4,
        stringsAsFactors = FALSE)

library(scdream)
data(sc.norm)
data(DistMap_coor)
#source("~/git/scdream/R/simi_dist_measures.R")
#source("~/git/scdream/R/dist_evaluation.R")
#source("~/git/scdream/scripts/mapping.R")

library(GA)
library(doParallel)
# registerDoParallel(cores = 20)

# pack = c("scatterplot3d", "rgl", "DistMap", "mccr")
# check_and_install_package(pack)
# check_and_install_package("devtools")
dat = sc.norm
nBits =  nrow(dat)
coor = DistMap_coor
library(Rtsne)

fitness.tsne.coor <- function(bin) {
	#tsne 3d corr with x/y/z
	s.i = which(bin == 1)
	X = t(dat[s.i, ])
	set.seed(123456789)
	dims = 5
	perplexity = 8
	max_iter = 2000
	theta = 0
	tsne  = errorcast <- try (Rtsne(
									X,
									dims = dims,
									perplexity = perplexity,
									verbose = T,
									max_iter = max_iter,
									theta = theta,
									))
	if (class(errorcast) == "try-error") {
		return (-1000000)
	}
	out = tsne$Y
	stopifnot(ncol(out) == dims)
	stopifnot(nrow(out) == nrow(coor))
	coor.cor = c()
	for (i in 1:3) {
		true.pos = coor[,i]
		r.v = c()
		for (j in 1:dims) {
			fit = cor.test(true.pos, out[,j])
			r.v = c(r.v, fit$estimate)
		}
		r.v.abs = max(abs(r.v), na.rm=T)
		coor.cor = c(coor.cor, r.v.abs)
	}
	mean(coor.cor, na.rm=T)
}

GA <- ga(
		 type = "binary",
		 fitness = fitness.tsne.coor,
		 #population = initial_population,
		 #selection = select_pop,
		 #crossover = crossover_pop,
		 #mutation = mutat_pop,
		 #monitor = monitor_check_genenum,
		 popSize = popSize,
		 keepBest = keepBest,
		 parallel = ifparallel,
		 maxiter = maxiter,
		 run = 	run,
		 seed = 10000,
		 nBits = nBits
		 )
summary(GA)
s = GA@solution
rowSums(s)
s = GA@population
rowSums(s)

save (GA, file=out.rdata.file)

pdf(out.pdf)
plot(GA)
dev.off()

