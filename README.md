## SCDream

SCDream is a R package for Dream Challenge on Single-Cell Transcriptomics

### Install PDdream package
```
library(devtools);
install_bitbucket(repo="omicsengineering/scdream")
```

### Pipeline

All the pipelines was on top of R statistical environment, and the scripts were put on scripts/ fold. 

1. preparing for datalist 
	`step1_prepare_rdata.R` 
2. calculating gene weight on sapce
	`step2_spacial_gene_weight.R`
3. grid searching tsne parameters
	`step3_grid_tsne_scdata_84gene.R`
4. dist cont method based gene selection
	`dist_count.R`	
5. Genetic Algorithm based gene selection
	`MLv5cvMRF_ga.R`
6. gene performance evaluation
	`eval_genes.R`
7. for post-dream test with 10 fold cv
	sub-challeage 1: `dist_cont_CV.R`
	sub-challeage 2: `dist_cont_CV.R`
	sub-challeage 3: `postMLv6cvMRF_ga.R`

